bugs::
	prebuffering
	modify TextEditorActivePtr and other LINKS if cut/paste/create/delete some before active editing cmd

todo::
	chorus/flanger
	hq reverb
	simple amp
	WIP drag&drop anything have [x,y] 

//--

opcode
params ?links
editor params
- label
- active
- selection
- expand (timeline ghost)


blockdata (opcode)
data
editor params
- selection

param
-static
-dynamic from shader
-dynamic from another cmd

data/param type
nums:byte sbyte sword int32
enums
enums (from shader)
enums (links):
shaders
textures
buffers
?

//----------------

struct cmd
{
params_editor
	char label[32]
	paramnames
	paramenums
params_runtime
	float4 pos
	char text[21]
	texture slot 1 
	buffer
	shader

methods editor
	read/write
	updatelinks?
	show in stack
methods runtime
	routine
}