using namespace DirectX; 

namespace dx
{
	void CommonInit();
	void CreateTempTexture();
	//------------------------------------------------------------
	void CreateConstBuf();

	int width; int height;
	float aspect;
	float iaspect;
	char* TextureTempMem;

	D3D_DRIVER_TYPE         g_driverType = D3D_DRIVER_TYPE_NULL;
	ID3D11Device*           g_pd3dDevice = NULL;
	ID3D11DeviceContext*    g_pImmediateContext = NULL;
	IDXGISwapChain*         g_pSwapChain = NULL;
	ID3D11RenderTargetView* g_pRenderTargetView = NULL;
	int currentRTn = -1;

	ID3D11Texture2D*        g_pDepthStencil = NULL;     // �������� ������ ������

	ID3D11DepthStencilView* g_pDepthStencilView = NULL;
	ID3D11ShaderResourceView* g_pDepthStencilSRView = NULL;
	ID3D11DepthStencilState * pDSState[4];
	

	ID3D11BlendState* bs[5*4];

	XMMATRIX View;
	XMMATRIX Projection;

#ifdef EditMode
	XMMATRIX tempView;
#endif

	void InitCamera()
	{
		XMVECTOR Eye = XMVectorSet(0.0f, 0.0f, -1.0f, 0.0f);
		XMVECTOR At = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);
		XMVECTOR Up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
		View = XMMatrixLookAtLH(Eye, At, Up);
		Projection = XMMatrixPerspectiveFovLH(XM_PIDIV4, width / (FLOAT)height, 0.01f, 100.0f);
	}

	HRESULT InitDevice()
	{
		HRESULT hr = S_OK;

		RECT rc;
		GetClientRect(hWnd, &rc);
		width = rc.right - rc.left;
		height = rc.bottom - rc.top;
		aspect = float(height) / float(width);
		iaspect = float(width) / float(height);

		DXGI_SWAP_CHAIN_DESC sd;
		ZeroMemory(&sd, sizeof(sd));
		sd.BufferCount = 1;
		sd.BufferDesc.Width = width;
		sd.BufferDesc.Height = height;
		sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		sd.BufferDesc.RefreshRate.Numerator = 60;
		sd.BufferDesc.RefreshRate.Denominator = 1;
		sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		sd.OutputWindow = hWnd;
		sd.SampleDesc.Count = 1;
		sd.SampleDesc.Quality = 0;
		sd.Windowed = TRUE;

		D3D_FEATURE_LEVEL featureLevels[] =
		{
			D3D_FEATURE_LEVEL_11_1,
			D3D_FEATURE_LEVEL_11_0,
			D3D_FEATURE_LEVEL_10_1,
			D3D_FEATURE_LEVEL_10_0,
		};

		UINT numFeatureLevels = ARRAYSIZE(featureLevels);

		hr = D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, 0, featureLevels, numFeatureLevels, D3D11_SDK_VERSION, &sd, &g_pSwapChain, &g_pd3dDevice, NULL, &g_pImmediateContext);
		if (FAILED(hr)) { MessageBox(hWnd, "device not created", "dx11error", MB_OK); return S_FALSE; }
		ID3D11Texture2D* pBackBuffer = NULL;
		hr = g_pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);
		if (FAILED(hr)) { MessageBox(hWnd, "swapchain error", "dx11error", MB_OK); return S_FALSE; }
		hr = g_pd3dDevice->CreateRenderTargetView(pBackBuffer, NULL, &g_pRenderTargetView);
		if (FAILED(hr)) { MessageBox(hWnd, "rt not created", "dx11error", MB_OK); return S_FALSE; }
		pBackBuffer->Release();



		D3D11_TEXTURE2D_DESC descDepth;    // ��������� � �����������
		ZeroMemory(&descDepth, sizeof(descDepth));
		descDepth.Width = width;        // ������ �
		descDepth.Height = height;        // ������ ��������
		descDepth.MipLevels = 1;        // ������� ������������
		descDepth.ArraySize = 1;
		descDepth.Format = DXGI_FORMAT_R32_TYPELESS;    // ������ (������ �������)
		descDepth.SampleDesc.Count = 1;
		descDepth.SampleDesc.Quality = 0;
		descDepth.Usage = D3D11_USAGE_DEFAULT;
		descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL| D3D11_BIND_SHADER_RESOURCE;        // ��� - ����� ������
		descDepth.CPUAccessFlags = 0;
		descDepth.MiscFlags = 0;
		// ��� ������ ����������� ���������-�������� ������� ������ ��������
		hr = g_pd3dDevice->CreateTexture2D(&descDepth, NULL, &g_pDepthStencil);
		if (FAILED(hr)) return hr;
		// ������ ���� ������� ��� ������ ������ ������
		D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;    // ��������� � �����������
		ZeroMemory(&descDSV, sizeof(descDSV));
		descDSV.Format = DXGI_FORMAT_D32_FLOAT;        // ������ ��� � ��������
		descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
		descDSV.Texture2D.MipSlice = 0;
		descDSV.Flags = 0;
		

		// ��� ������ ����������� ���������-�������� � �������� ������� ������ ������ ������
		hr = g_pd3dDevice->CreateDepthStencilView(g_pDepthStencil, &descDSV, &g_pDepthStencilView);
		if (FAILED(hr)) return hr;

		D3D11_SHADER_RESOURCE_VIEW_DESC sr_desc;
		sr_desc.Format = DXGI_FORMAT_R32_FLOAT;
		sr_desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		sr_desc.Texture2D.MostDetailedMip = 0;
		sr_desc.Texture2D.MipLevels = 1;
	
	
		hr = g_pd3dDevice->CreateShaderResourceView(g_pDepthStencil, &sr_desc, &g_pDepthStencilSRView);
		if (FAILED(hr)) return hr;

		// ���������� ������ ������� ������ � ������ ������ ������ � ��������� ����������
		g_pImmediateContext->OMSetRenderTargets(1, &g_pRenderTargetView, g_pDepthStencilView);
//		g_pImmediateContext->OMSetRenderTargets(1, &g_pRenderTargetView, 0);

		// Setup the viewport
		D3D11_VIEWPORT vp;
		vp.Width = (FLOAT)width;
		vp.Height = (FLOAT)height;
		vp.MinDepth = 0.0f;
		vp.MaxDepth = 1.0f;
		vp.TopLeftX = 0;
		vp.TopLeftY = 0;
		g_pImmediateContext->RSSetViewports(1, &vp);
		
		ID3D11RasterizerState * g_pRasterState;

		D3D11_RASTERIZER_DESC rasterizerState;
		rasterizerState.FillMode = D3D11_FILL_SOLID;
		rasterizerState.CullMode = D3D11_CULL_NONE;
		rasterizerState.FrontCounterClockwise = true;
		rasterizerState.DepthBias = false;
		rasterizerState.DepthBiasClamp = 0;
		rasterizerState.SlopeScaledDepthBias = 0;
		rasterizerState.DepthClipEnable = false;
		rasterizerState.ScissorEnable = false;
		rasterizerState.MultisampleEnable = false;
		rasterizerState.AntialiasedLineEnable = true;
		g_pd3dDevice->CreateRasterizerState(&rasterizerState, &g_pRasterState);

		g_pImmediateContext->RSSetState(g_pRasterState);

		
		D3D11_BLEND_DESC bSDesc;
		ZeroMemory(&bSDesc, sizeof(D3D11_BLEND_DESC));
		bSDesc.RenderTarget[0].BlendEnable = TRUE;
		bSDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
		bSDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
		bSDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
		bSDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ZERO;
		bSDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
		bSDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
		bSDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

		bSDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
		bSDesc.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
		for (int i = 0; i < 5; i++)
		{
			bSDesc.RenderTarget[0].BlendOp = (D3D11_BLEND_OP)(i+1);
			g_pd3dDevice->CreateBlendState(&bSDesc, &bs[i]);
		}

		bSDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
		bSDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
		for (int i = 0; i < 5; i++)
		{
			bSDesc.RenderTarget[0].BlendOp = (D3D11_BLEND_OP)(i + 1);
			g_pd3dDevice->CreateBlendState(&bSDesc, &bs[i+5]);
		}

		bSDesc.RenderTarget[0].BlendEnable = TRUE;
		bSDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
		bSDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
		bSDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
		bSDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
		bSDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
		bSDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
		bSDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
		for (int i = 0; i < 5; i++)
		{
			bSDesc.RenderTarget[0].BlendOp = (D3D11_BLEND_OP)(i + 1);
			g_pd3dDevice->CreateBlendState(&bSDesc, &bs[i + 5*2]);
		}
		
		bSDesc.RenderTarget[0].BlendEnable = FALSE;
		bSDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
		bSDesc.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
		bSDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
		bSDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ZERO;
		bSDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
		bSDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
		bSDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
		for (int i = 0; i < 5; i++)
		{
			bSDesc.RenderTarget[0].BlendOp = (D3D11_BLEND_OP)(i + 1);
			g_pd3dDevice->CreateBlendState(&bSDesc, &bs[i + 5 * 3]);
		}

		D3D11_DEPTH_STENCIL_DESC dsDesc;
		// Depth test parameters
		dsDesc.DepthEnable = false;
		dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
		dsDesc.DepthFunc = D3D11_COMPARISON_LESS;

		// Stencil test parameters
		dsDesc.StencilEnable = true;
		dsDesc.StencilReadMask = 0xFF;
		dsDesc.StencilWriteMask = 0xFF;

		// Stencil operations if pixel is front-facing
		dsDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		dsDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
		dsDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

		// Stencil operations if pixel is back-facing
		dsDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		dsDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
		dsDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		dsDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

		// Create depth stencil state
		dsDesc.StencilEnable = false;

		g_pd3dDevice->CreateDepthStencilState(&dsDesc, &pDSState[0]);//off

		dsDesc.DepthEnable = true;
		dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
		g_pd3dDevice->CreateDepthStencilState(&dsDesc, &pDSState[1]);//read & write

		dsDesc.DepthEnable = true;
		dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
		g_pd3dDevice->CreateDepthStencilState(&dsDesc, &pDSState[2]);//read

		dsDesc.DepthEnable = false;
		dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
		g_pd3dDevice->CreateDepthStencilState(&dsDesc, &pDSState[3]);//write

		CreateConstBuf();


		CommonInit();
		InitCamera();
		

		TextureTempMem = (char *)malloc(2048 * 2048 * 4 * 2);
		CreateTempTexture();
		return S_OK;
	}


	//--------------------------------------------------------------------------------------
	// Clean up the objects we've created
	//--------------------------------------------------------------------------------------
	void CleanupDevice()
	{
		if (g_pImmediateContext) g_pImmediateContext->ClearState();
		if (g_pRenderTargetView) g_pRenderTargetView->Release();
		if (g_pSwapChain) g_pSwapChain->Release();
		if (g_pImmediateContext) g_pImmediateContext->Release();
		if (g_pd3dDevice) g_pd3dDevice->Release();
	}



	//-----------------------------------------
	//shaders support

	typedef struct {
		ID3D11VertexShader*     pVs;
		ID3D11PixelShader*      pPs;
		ID3DBlob*				pVSBlob;
		ID3DBlob*				pPSBlob;
	} shaderStruct;

	shaderStruct Shader[255];

	struct Vertex
	{
		XMFLOAT3 Pos;
		XMFLOAT2 Tex;
		XMFLOAT2 Tex1;
	};

	struct ConstantBuffer
	{
		XMMATRIX Model;
		XMMATRIX View;
		XMMATRIX Proj;
		XMFLOAT4 time;
		XMFLOAT4 aspectRatio;
		XMFLOAT4 params[MAXPARAM];
	};

	ID3D11Buffer*			ppVB;
	ID3D11InputLayout*      ppLayout;


	ID3D11InputLayout*      pVertexLayout[256];
	ID3D11Buffer*			pVertexBuffer[256];

	ID3D11Buffer*           pConstantBufferV;
	ID3D11Buffer*           pConstantBufferP;

	//custom constant buffers (command-depending)
	ID3D11Buffer*           pCustomConstantBufferV_1;
	ID3D11Buffer*           pCustomConstantBufferP_1;

	//for particles type 1
	ID3D11Buffer*			pParticleCB_t1V;
	ID3D11Buffer*			pParticleCB_t1P;

	//ID3D11ShaderResourceView* Texture[256];
	
	

	struct ParticleParams
	{
		XMFLOAT4 EmitterPos;
		XMFLOAT4 EmitterSize;
		XMFLOAT4 EmitterType;
		XMFLOAT4 MaxParticleCount;
		
		XMFLOAT4 Position;
		XMFLOAT4 PositionRange;
		
		XMFLOAT4 Size;
		XMFLOAT4 SizeRange;
		
		XMFLOAT4 Color;
		XMFLOAT4 ColorRange;
		
		XMFLOAT4 Velocity;
		XMFLOAT4 VelocityRange;
		
		XMFLOAT4 Acceleration;
		XMFLOAT4 AccelerationRange;

		XMFLOAT4 Rotation;
		XMFLOAT4 RotationRange;
		XMFLOAT4 RotationPhase;
		XMFLOAT4 RotationPhaseRange;
		XMFLOAT4 RotationOffset;

		XMFLOAT4 ColorOverLife[5];
		XMFLOAT4 ColorOverLifePos[5];
		XMFLOAT4 SizeOverLife[5];
		XMFLOAT4 cpCount;

		XMFLOAT4 Basis;

		XMFLOAT4 MotionBlur;
		XMFLOAT4 Deaccel;

		XMFLOAT4 Start;
		XMFLOAT4 End;
		XMFLOAT4 LifeTime;
		XMFLOAT4 PPS;

		XMFLOAT4 PosBufferSize;
		XMFLOAT4 PosTable[260];
	};

	struct ParticleParamsP
	{
		XMFLOAT4 evoSpeed;//start spd,end spd,same for alpha
		XMFLOAT4 evoFold;// rgb/a
		XMFLOAT4 scrollSpeed;//start spd,end spd,same for alpha
		XMFLOAT4 scrollDir;
	};

	struct CustomConstantBufferV_1
	{
		XMFLOAT4 point[320];
	};

	struct CustomConstantBufferP_1
	{
		XMFLOAT4 color;
	};
	//-----------------------

	ID3D11Texture2D *pTexture[256];

	#define tempTexCount 7*3+1
	ID3D11Texture2D *temptexture[tempTexCount];
	ID3D11ShaderResourceView* TempTextureResView[tempTexCount];

	ID3D11ShaderResourceView* TextureResView[256];
	ID3D11RenderTargetView* RenderTargetView[256];
	XMFLOAT2 Tsize[256];
	BYTE TFormat[256];
	ID3D11SamplerState*       pSamplerLinear = NULL;
	ID3D11SamplerState*       pSamplerNone = NULL;

	ID3DBlob* pErrorBlob;

	void CompileShader(int n, char* source_vs, char* source_ps)
	{
	/*	int sizeVS = 0;
		while (*(BYTE*)(source_vs + sizeVS) != 0) sizeVS++;
		int sizePS = 0;
		while (*(BYTE*)(source_ps + sizePS) != 0) sizePS++;
		*/
		HRESULT hr = S_OK;
		//return;

		if (Shader[n].pVSBlob) Shader[n].pVSBlob->Release();

		hr= D3DCompile(source_vs,strlen(source_vs),NULL, NULL, NULL, "VS","vs_4_0", D3DCOMPILE_PACK_MATRIX_ROW_MAJOR, NULL, &Shader[n].pVSBlob, &pErrorBlob);
		if (FAILED(hr)) {/* SetWindowText(hWnd, (char*)pErrorBlob->GetBufferPointer());*/ return; }

		if (Shader[n].pPSBlob) Shader[n].pPSBlob->Release();
		hr = D3DCompile(source_ps, strlen(source_vs), NULL, NULL, NULL, "PS", "ps_4_0", NULL, NULL, &Shader[n].pPSBlob, &pErrorBlob);
		if (FAILED(hr)) {/* SetWindowText(hWnd, (char*)pErrorBlob->GetBufferPointer()); */return; }

		if (Shader[n].pVs) Shader[n].pVs->Release();
		hr = g_pd3dDevice->CreateVertexShader(Shader[n].pVSBlob->GetBufferPointer(), Shader[n].pVSBlob->GetBufferSize(), NULL, &Shader[n].pVs);
		if (FAILED(hr)) { SetWindowText(hWnd, "vs fail");  return; }

		if (Shader[n].pPs) Shader[n].pPs->Release();
		hr = g_pd3dDevice->CreatePixelShader(Shader[n].pPSBlob->GetBufferPointer(), Shader[n].pPSBlob->GetBufferSize(), NULL, &Shader[n].pPs);
		if (FAILED(hr)) { SetWindowText(hWnd, "ps fail");  return; }

		//SetWindowText(hWnd, "Shader compiled");

		D3D11_INPUT_ELEMENT_DESC layout[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD",1, DXGI_FORMAT_R32G32_FLOAT, 0, 20, D3D11_INPUT_PER_VERTEX_DATA, 0 }
		};
		UINT numElements = ARRAYSIZE(layout);

		if (pVertexLayout[n]!=NULL) pVertexLayout[n]->Release();

		hr = dx::g_pd3dDevice->CreateInputLayout(layout, numElements, Shader[n].pVSBlob->GetBufferPointer(), Shader[n].pVSBlob->GetBufferSize(), &pVertexLayout[n]);
	//	Shader[n].pVSBlob->Release();
	//	Shader[n].pPSBlob->Release();
		if (FAILED(hr)) { SetWindowText(hWnd, "input layout fail");  return; }

	//	g_pImmediateContext->IASetInputLayout(pVertexLayout);
	}

	int roundUp(int n, int r)
	{
		return 	n - (n % r) + r;
	}



	void CreateConstBuf()
	{
		//common buffers
		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = roundUp(sizeof(ConstantBuffer), 16);
		bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bd.CPUAccessFlags = 0;
		bd.StructureByteStride = 16;

		HRESULT hr = g_pd3dDevice->CreateBuffer(&bd, NULL, &pConstantBufferV);
		if (FAILED(hr)) { SetWindowText(hWnd, "constant bufferV fail");  return; }

		bd.ByteWidth = roundUp(sizeof(ConstantBuffer), 16);
		HRESULT hr2 = g_pd3dDevice->CreateBuffer(&bd, NULL, &pConstantBufferP);
		if (FAILED(hr2)) { SetWindowText(hWnd, "constant bufferP fail");  return; }

		//custom buffers
	/*	ZeroMemory(&bd, sizeof(bd));
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = roundUp(sizeof(CustomConstantBufferV_1), 16);
		bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bd.CPUAccessFlags = 0;
		bd.StructureByteStride = 16;

		hr = g_pd3dDevice->CreateBuffer(&bd, NULL, &pCustomConstantBufferV_1);
		if (FAILED(hr)) { SetWindowText(hWnd, "constant bufferV fail");  return; }

		bd.ByteWidth = roundUp(sizeof(CustomConstantBufferP_1), 16);
		hr2 = g_pd3dDevice->CreateBuffer(&bd, NULL, &pCustomConstantBufferP_1);
		if (FAILED(hr2)) { SetWindowText(hWnd, "constant bufferP fail");  return; }*/

		//particle container1 custom constant buffers
		ZeroMemory(&bd, sizeof(bd));
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = roundUp(sizeof(ParticleParams), 16);
		bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bd.CPUAccessFlags = 0;
		bd.StructureByteStride = 16;

		hr = g_pd3dDevice->CreateBuffer(&bd, NULL, &pParticleCB_t1V);
		if (FAILED(hr)) { SetWindowText(hWnd, "constant bufferV fail");  return; }

		bd.ByteWidth = roundUp(sizeof(ParticleParamsP), 16);
		hr2 = g_pd3dDevice->CreateBuffer(&bd, NULL, &pParticleCB_t1P);
		if (FAILED(hr2)) { SetWindowText(hWnd, "constant bufferP fail");  return; }

	}

#define p_count 48000
	Vertex* vertices = NULL;
	Vertex* pp = NULL;

	int VBsize[256];
	void CreatePP();


	//ID3D11SamplerState*       g_pSamplerLinear = NULL;
	void SetTextureOptions()
	{
		D3D11_SAMPLER_DESC sampDesc;
		ZeroMemory(&sampDesc, sizeof(sampDesc));
		sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
		sampDesc.MinLOD = 0;
		sampDesc.MaxLOD =  D3D11_FLOAT32_MAX;
							// ������� ��������� ������ ���������������
		HRESULT h=dx::g_pd3dDevice->CreateSamplerState(&sampDesc, &dx::pSamplerLinear);
		

		sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
		sampDesc.MinLOD = 0;
		sampDesc.MaxLOD = 0;
		// ������� ��������� ������ ���������������
		 h = dx::g_pd3dDevice->CreateSamplerState(&sampDesc, &dx::pSamplerNone);

	}

	void CommonInit()
	{
//		vertices = (Vertex *)malloc(sizeof(Vertex)*p_count * 6);
		pp= (Vertex *)malloc(sizeof(Vertex) * 6);
		CreatePP();
		dx::SetTextureOptions();
	}

	void CreateVB(int n,int size)
	{
		return;
		VBsize[n] = size*6;

		for (int i = 0; i < p_count; i++)
		{
			vertices[i * 6 + 0].Tex = XMFLOAT2(0, 0);
			vertices[i * 6 + 1].Tex = XMFLOAT2(1, 0);
			vertices[i * 6 + 2].Tex = XMFLOAT2(1, 1);
			vertices[i * 6 + 3].Tex = XMFLOAT2(0, 0);
			vertices[i * 6 + 4].Tex = XMFLOAT2(1, 1);
			vertices[i * 6 + 5].Tex = XMFLOAT2(0, 1);

			for (int x = 0; x < 6; x++)
			{
				vertices[i * 6 + x].Pos = XMFLOAT3(0, 0, 0);
				vertices[i * 6 + x].Tex1 = XMFLOAT2((float)i, 0);
			}
		}

		HRESULT hr = S_OK;

		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(Vertex) * p_count * 6;
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bd.CPUAccessFlags = 0;
		D3D11_SUBRESOURCE_DATA InitData;
		ZeroMemory(&InitData, sizeof(InitData));
		InitData.pSysMem = vertices;

		if (pVertexBuffer[n] != NULL) pVertexBuffer[n]->Release();
		hr = g_pd3dDevice->CreateBuffer(&bd, &InitData, &pVertexBuffer[n]);
	}

	void CreatePP()
	{

		
			pp[0].Tex = XMFLOAT2(0, 0);
			pp[1].Tex = XMFLOAT2(1, 0);
			pp[2].Tex = XMFLOAT2(1, 1);
			pp[3].Tex = XMFLOAT2(0, 0);
			pp[4].Tex = XMFLOAT2(1, 1);
			pp[5].Tex = XMFLOAT2(0, 1);

			for (int x = 0; x < 6; x++)
			{
				pp[x].Pos = XMFLOAT3(0, 0, 0);
				pp[x].Tex1 = XMFLOAT2(0, 0);
			}
		

		HRESULT hr = S_OK;

		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(Vertex) * 6;
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bd.CPUAccessFlags = 0;
		D3D11_SUBRESOURCE_DATA InitData;
		ZeroMemory(&InitData, sizeof(InitData));
		InitData.pSysMem = pp;

		if (ppVB != NULL) ppVB->Release();
		hr = g_pd3dDevice->CreateBuffer(&bd, &InitData, &ppVB);
	}

	void CreateTempTexture()
	{

		int mip = 0;
	
		int size = 32;

		D3D11_TEXTURE2D_DESC tdesc;
		D3D11_SUBRESOURCE_DATA tbsd;

		tbsd.pSysMem = (void *)dx::TextureTempMem;
		tbsd.SysMemPitch = size * 4;
		tbsd.SysMemSlicePitch = size * size * 4;

		tdesc.Width = size;
		tdesc.Height = size;

		tdesc.SampleDesc.Count = 1;
		tdesc.SampleDesc.Quality = 0;
		tdesc.Usage = D3D11_USAGE_DEFAULT;

		//tdesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;

		tdesc.CPUAccessFlags = 0;


		tdesc.MipLevels = 0;
		tdesc.ArraySize = 1;
		tdesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;
		tdesc.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS;

		D3D11_SHADER_RESOURCE_VIEW_DESC svDesc;

		
		svDesc.Texture2D.MipLevels = 1;
		svDesc.Texture2D.MostDetailedMip = 0;
		svDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;

		for (int f = 0; f < 3; f++)
		{
			switch (f)
			{
			case 0:tdesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM; break;
			case 1:tdesc.Format = DXGI_FORMAT_R16G16B16A16_FLOAT; break;
			case 2:tdesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT; break;
			}
			svDesc.Format = tdesc.Format;

			for (int x = 0; x < (tempTexCount - 1)/3; x++)
			{
				tdesc.Width = size;
				tdesc.Height = size;

				HRESULT h = dx::g_pd3dDevice->CreateTexture2D(&tdesc, NULL , &dx::temptexture[x+f*7]);
				h = dx::g_pd3dDevice->CreateShaderResourceView(dx::temptexture[x+f*7], &svDesc, &dx::TempTextureResView[x+f*7]);

				size *= 2;
			}
		}
		
		tdesc.MipLevels = 1;
		tdesc.ArraySize = 1;
		tdesc.MiscFlags = 0;

		tdesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		svDesc.Format = tdesc.Format;
		tdesc.Width = dx::width;
		tdesc.Height = dx::height;
		HRESULT h = dx::g_pd3dDevice->CreateTexture2D(&tdesc, &tbsd, &dx::temptexture[tempTexCount-1]);
		h = dx::g_pd3dDevice->CreateShaderResourceView(dx::temptexture[tempTexCount-1], &svDesc, &dx::TempTextureResView[tempTexCount-1]);


		/*D3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc;

		renderTargetViewDesc.Format = tdesc.Format;
		renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
		renderTargetViewDesc.Texture2D.MipSlice = 0;
		*/
		// Create the render target view.
		//HRESULT result = dx::g_pd3dDevice->CreateRenderTargetView(dx::TempTexture, &renderTargetViewDesc, &dx::RenderTargetView[tempTexCounter]);

		//tempTexCounter++;

	}

}